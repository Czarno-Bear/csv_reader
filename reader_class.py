import os
import csv

'''
Klasa ChangerCsv pobiera sciezke z plikiem, sciezke docelowa pliku, oraz 
wspolrzedne i wartosc.

Funkcja is_path_file_exists sprawdza istnienie pliku, jesli nie, to wyswietli
stosowny komunikat. Jesli tak to zwroci True.

Funkcja  open_file Otwiera plik oraz tworzy zagnierzdzona liste.

Funkcja change_file_data zmienia wartosci pod podanymi wspolrzednymi.

Funkcja is_path_to_exists sprawdza, czy foldery w katalogu roboczym są obecne. 
Jesli nie, to tworzy je i zapisuje plik.

Funkja save_file zapisuje plik pod podana sciezka.
'''


class ChangerCsv:

    def __init__(self, path_from, path_to, arguments):
        self.path_from = path_from
        self.path_to = path_to
        self.arguments = arguments
        self.csv_base = []
        self.y_line = 0
        self.x_element = 0

    def is_path_file_exists(self):

        is_file_exist = os.path.exists(self.path_from)
        show_dir = os.listdir()
        if is_file_exist:
            return True
        else:
            return print('Blad! W tym folderze znajduja sie '
                         'nastepujace pliki/foldery: {}'.format(show_dir))

    def open_file(self):
        with open(self.path_from, 'r') as f:
            reader = csv.reader(f)
            for line in reader:
                clean_line = []
                for element in line:
                    clean_line.append(element.strip())
                self.csv_base.append(clean_line)
                self.y_line = len(self.csv_base)
                self.x_element = len(clean_line)
            return self.csv_base, self.y_line, self.x_element

    def change_file_data(self):
        idx = 0
        quantity = len(self.arguments)
        while True:
            if idx < quantity:
                y = int(self.arguments[idx])
                x = int(self.arguments[idx + 1])
                value = self.arguments[idx + 2]
                if y > self.y_line or x > self.x_element:
                    print('Podano za wysokie wspolrzedne!')
                    break
                else:
                    self.csv_base[y][x] = value
            else:
                break
            idx += 3

    def is_path_to_exists(self):
        if os.path.exists(self.path_to):
            return True
        else:
            folder = os.path.split(self.path_to)[0]
            os.makedirs(folder)
            return True

    def save_file(self):
        with open(self.path_to, 'w') as f:
            writer = csv.writer(f, delimiter=',', quotechar=' ')
            for line in self.csv_base:
                writer.writerow(line)
